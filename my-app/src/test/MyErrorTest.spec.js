import React from "react";
import { shallow, mount } from 'enzyme';
import MyError from '../components/MyError';
import toJson from 'enzyme-to-json';
import ButtonCustom from "../components/ButtonCustom";
import SimpleForm from "../components/formEx";
import { wrap } from "module";
import SearchBar from "../components/SearchBar";

describe('MyError',()=>{

    const minProps = {
        buttons: '',
        item: '',
        color: '',
        click: ()=>{}
    };


    it('renders Error Messsage without problems',()=>{
        expect(shallow(<MyError/>)).toHaveLength(1);
    });

    it('renders an error message', ()=>{
        expect(toJson(shallow(<MyError/>))).toMatchSnapshot();
    });

    it('renders buttons navBar',()=>{
        const onClick=jest.fn();
        let buttons = ['Cena', 'DuasCenas', 'VariasCenas'];
        const wrapper = shallow(<ButtonCustom {...minProps} buttons={buttons} click={onClick} />);

        //fire a click eent on the button element
        wrapper.find('Button').simulate('click');
           // console.log(wrapper.find('ButtonCustom'));
       // expect(wrapper.find('Button').ButtonCustom.props);
        expect(onClick).toHaveBeenCalled();

    });

    it('updates values on input change',()=>{
        const wrapper = shallow(<SimpleForm submit={()=>{}}/>);

       wrapper.find('input').simulate('change', {
           target: {value: 'new value'}
       });

       expect(wrapper.state().input).toBe('new value');

    });

    it('calls form handler on submit',()=>{
        const mockHandler = jest.fn();

        const wrapper = shallow(<SimpleForm submit={mockHandler} />);
        wrapper.setState({input: 'the form value'});

        wrapper.find('form').simulate('submit', {preventDefault: jest.fn()});
        
        expect(mockHandler).toHaveBeenCalledWith('the form value');
    });

    it('updates values on input change',()=>{
        const wrapper = shallow(<SearchBar/> );
    })
});