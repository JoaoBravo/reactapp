import React from "react";
import NavBar from "./components/NavBar.jsx";
import logo from "./logo.svg";
import "./App.css";
import SearchBar from "./components/SearchBar";
import MenuBar from "./components/MenuBar";
import { Switch, Route } from "react-router-dom";
import HomePage from "./components/HomePage";

function App() {
  return (
    <div>
      <MenuBar />
      <img src={logo} className="App-logo" alt="logo" />
      <div className="App">
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route path="/projects" component={NavBar} />
          <Route path="/devProfile" component={SearchBar} />
        </Switch>
      </div>
    </div>
  );
}

export default App;
