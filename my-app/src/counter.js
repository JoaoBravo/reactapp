import React, { Component } from "react";

class Counter extends Component {
  state = { value: 0 };

  stateUpdateCb() {
    console.log("New state is ", this.state.value);
  }

  reset = () => {
    this.setState({ value: 0 }, this.stateUpdateCb);
  };
  increment = () => {
    this.setState(
      prevState => ({ value: prevState.value + 1 }),
      this.stateUpdateCb
    );
  };

  render() {
   return (
       <div>
           <button onClick={this.increment}>{this.state.value}</button>
           <button onClick={this.reset}> Reset</button>
       </div>
   
   );
  }
}

export default Counter;
