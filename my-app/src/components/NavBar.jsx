import React, { Component } from "react";
import ButtonCustom from "./ButtonCustom";
import { Button } from "semantic-ui-react";
import CardContainer from "./CardContainer";

class NavBar extends Component {
  static languages = [
    "All",
    "javascript",
    "java",
    "html",
    "css",
    "ruby",
    "c",
    "c++",
    "python"
  ];
  state = { selected: "All" };

  selectLanguage(lang) {
    this.setState({ selected: lang });
  }

  render() {
    return (
      <div>
        <Button.Group>
          {NavBar.languages.map(lang => {
            let color = lang === this.state.selected ? "red" : "black";
            return (
              <ButtonCustom
                click={() => this.selectLanguage(lang)}
                key={lang}
                item={lang}
                color={color}
              />
            );
          })}
        </Button.Group>
        <CardContainer key={this.state.selected} lang={this.state.selected} />
      </div>
    );
  }
}

export default NavBar;
