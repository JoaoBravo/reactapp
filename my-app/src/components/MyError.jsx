import React from "react";
import { Message, Icon } from "semantic-ui-react";
import "../App.css";

const MyError = () => (
  <div className="ErrorCard">
    <Message icon>
      <Icon name="circle notched" loading />
      <Message.Content>
        <Message.Header>Just one second</Message.Header>
        We are having some internet problems!
      </Message.Content>
    </Message>
  </div>
);

export default MyError;
