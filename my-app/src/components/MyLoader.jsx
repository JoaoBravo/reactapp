import React from "react";
import { Dimmer, Loader } from "semantic-ui-react";
import "../App.css";

const MyLoader = () => (
  <div>
    <Dimmer active inverted>
      <Loader size="large">Loading</Loader>
    </Dimmer>
  </div>
);

export default MyLoader;
