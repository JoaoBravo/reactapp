import React, { Component } from "react";
import { getUser } from "../services/Service";
import MyError from "./MyError";
import MyLoader from "./MyLoader";
import MyCard from "./MyCard";
import DummyCard from "./DummyCard";
import { Input, Container, Button} from "semantic-ui-react";
import SimpleForm from "./formEx";

class SearchBar extends Component {
  state = { 
      text: "" ,
      loading: true,
      error: null,
      card: null
    };

  async getInfo(){
      const data = await getUser(this.state.text);
      this.setState({card: data.data});
      this.setState({loading: false});

  }

  handleChange = event => {
      this.setState({text: event.target.value})
  };

  handleSubmit = () => {
      this.getInfo().catch(error => this.setState({error}));
  };

  render() {
      const {card, loading, error}=this.state;
      if(error){
        return <MyError/>;
      }
      /* if(loading){
        return <MyLoader/>;
      } */ else{
        return (
            <div>
                <h2>Git Username</h2>
              <Input
                focus placeholder='Search...'
                type="text"
                value={this.state.text}
                onChange={this.handleChange}
              />
              <Button color="blue" onClick={this.handleSubmit}>Submit</Button>
              <Container className="Card" >
              {card !== null ? <DummyCard card={card} key={card.id}/> : <div/> } 
              </Container>
            </div>
          );
      }
  }
}

export default SearchBar;
