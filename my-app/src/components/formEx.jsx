import React  from 'react';

class SimpleForm extends React.Component {
    //static propTypes = { submit: PropTypes.func.isRequired }; 
    state = { input: '' };
    handleChange = event => this.setState({ input: event.target.value });
    
    onSubmit(event) {
        event.preventDefault();
        this.props.submit(this.state.input);
    }
    
    render() {
        return (
            <div>
                <form onSubmit={(event) => {this.onSubmit(event)}}>
                    <input onChange={this.handleChange} value={this.state.input} />
                    <button type="submit">Click Me</button>
                </form>
            </div>
        );
    }
}

export default SimpleForm;