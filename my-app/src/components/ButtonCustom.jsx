import React from "react";
import { Button } from "semantic-ui-react";

function ButtonCustom(props) {
  return (
    <Button className={props.color} onClick={props.click}>
      {props.item.toUpperCase()}
    </Button>
  );
}


export default ButtonCustom;
