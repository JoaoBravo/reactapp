import React from "react";
import { Card, Icon, Image } from "semantic-ui-react";

function MyCard(props) {
  return (
    <Card>
      <Image src={props.card.owner.avatar_url} />
      <Card.Content>
        <Card.Header>{props.card.name}</Card.Header>
        <Card.Meta>
          <span className="date">Joined in {props.card.created_at}</span>
        </Card.Meta>
        <Card.Description>{props.card.stargazers_count}</Card.Description>
        <Card.Description>{props.card.language}</Card.Description>
      </Card.Content>
      <Card.Content extra>
        <a>
          <Icon name="linkify" />
          {props.card.clone_url}
        </a>
      </Card.Content>
    </Card>
  );
}

export default MyCard;
