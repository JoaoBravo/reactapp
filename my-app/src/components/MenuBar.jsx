import React, { Component } from "react";
import { Menu } from "semantic-ui-react";
import { Link } from "react-router-dom";

class MenuBar extends Component {
  state = {};
  handleItemClick = (e, { name }) => this.setState({ activeItem: name });

  render() {
    const { activeItem } = this.state;
    return (
      <Menu inverted>
        <Menu.Item
          name="home"
          active={activeItem === "home"}
          onClick={this.handleItemClick}
          as={Link}
          to="/"
        >
          Home
        </Menu.Item>

        <Menu.Item
          name="projects"
          active={activeItem === "projects"}
          onClick={this.handleItemClick}
          as={Link}
          to="/projects"
        >
          Popular Projects
        </Menu.Item>

        <Menu.Item
          name="devInfo"
          active={activeItem === "devInfo"}
          onClick={this.handleItemClick}
          as={Link}
          to="/devProfile"
        >
          Developer Info
        </Menu.Item>
      </Menu>
    );
  }
}

export default MenuBar;
