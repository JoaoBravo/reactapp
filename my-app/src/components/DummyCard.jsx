import React from "react";
import { Card, Icon, Image } from "semantic-ui-react";
import "../App.css";

function DummyCard(card) {
  return (
    <Card style= {{margin: "auto" }}>
     <Image src={card.card.avatar_url} />
      <Card.Content>
        <Card.Header>{card.card.name} ({card.card.login})</Card.Header>
        <Card.Meta>
          <span className="date">Joined in {card.card.created_at}</span>
        </Card.Meta>
        <Card.Description>{card.card.followers}</Card.Description>
      </Card.Content>
      <Card.Content extra>
        <a>
          <Icon name="linkify" />
          {card.card.url}
        </a>
      </Card.Content>
    </Card>
  );
}

export default DummyCard;
