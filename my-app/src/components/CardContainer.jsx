import React, { Component } from "react";
import MyCard from "./MyCard";
import { Grid, GridColumn } from "semantic-ui-react";
import "../App.css";
import MyLoader from "./MyLoader";
import MyError from "./MyError";
import { getRepos } from "../services/Service";

class CardContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cards: [],
      error: null,
      loading: true
    };
  }

  async getInfo() {
    const data = await getRepos(this.props);
    const cards = data.data.items;
    //console.log(cards);
    this.setState({ cards });
    this.setState({ loading: false });
  }

  async componentDidMount() {
    console.log(this.props.lang);
    await this.getInfo().catch(error => this.setState({ error }));

    /* axios
      .get(
        `https://api.github.com/search/repositories?q=stars:%3E1+language:${
          this.props.lang
        }&sort=stars&order=desc&type=Repositories`
      )
      .then(response => {
        const cards = response.data.items;
        this.setState({ cards });
        this.setState({ loading: false });
        //console.log(cards);
      })
      .catch(error => this.setState({ error })); */
  }

  render() {
    const { cards, loading, error } = this.state;
    if (error) {
      return <MyError />;
    }
    if (loading) {
      return <MyLoader />;
    } else {
      return (
        <div className="Border">
          <Grid relaxed columns={4}>
            {cards.map(card => {
              return (
                <GridColumn key={card.id}>
                  <MyCard card={card} />
                </GridColumn>
              );
            })}
          </Grid>
        </div>
      );
    }
  }
}

export default CardContainer;
